export EDITOR=nvi
export PATH=/usr/local/sbin:$PATH:/usr/local/bin:~/bin

if command -v "rbenv"; then
  eval "$(rbenv init - sh)"
  export PATH="$HOME/.rbenv/bin:$PATH"
fi

export HISTCONTROL=ignoredups:erasedups
export HISTSIZE=100000
export HISTFILESIZE=100000
shopt -s histappend

export PS1='\[\033[38;5;210m\]\w\[\033[00m\] \$ '

ssh-add -K ~/.ssh/id_rsa

alias st='open -a Sublime\ Text.app'

alias nz='COUNTRY=nz'
alias au='COUNTRY=au'
alias uk='COUNTRY=uk'
alias be='bundle exec'
[ -f ~/.ssh/id_rsa_flux ] && ssh-add -K ~/.ssh/id_rsa_flux

function lint_rb() {
    rubocop $(git st --porcelain=v1 | awk '{print $2}' | tr '\n' ' ')
}
