set showmode
set showmatch
set ruler
set shiftwidth=2
set tabstop=2
set verbose
set leftright
set cedit=\	
set filec=\	

map gr :%!prettier --parser=ruby --write
map gj :%!prettier --parser=babel --write
map gc :%!prettier --parser=css --write
map gJ :%!prettier --parser=json --write
map gh :%!prettier --parser=html --write
