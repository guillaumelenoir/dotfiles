set nocompatible
set number
set laststatus=2
set ruler
set hlsearch

set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent

set backspace=2
set tw=120
au FileType gitcommit set tw=72

au FileType html setlocal sw=2 sts=2
au FileType ruby setlocal sw=2 sts=2
au FileType js setlocal sw=2 sts=2
au FileType php setlocal sw=4 sts=4
