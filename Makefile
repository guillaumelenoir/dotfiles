.PHONY:all git vim bash tmux nvi

all: git vim bash tmux

git:
	rm -f ~/.gitconfig ~/.gitignore_global
	cp .gitconfig .gitignore_global ~

vim:
	rm -rf ~/.vim ~/.vimrc
	cp .vimrc ~

bash:
	rm -rf ~/.bashrc ~/.profile
	cp .bashrc ~

tmux:
	rm -rf ~/.tmux.conf
	cp .tmux.conf ~

nvi:
	rm -rf ~/.exrc
	cp .exrc ~
